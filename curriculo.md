# Paulo Celso de Souza Panissa Filho
Brasileiro, Casado, 31 Anos

E-mail: p.panissa@gmail.com

Telefone: 67 9 9810-1392


### Objetivo
Atuar como Desenvolvedor Front-end
 
### Formação
2018 - Cursando Análise e Desenvolvimento de Sistemas

### Experiência Profissional
** 02/2019 ** - **até momento** - [Before TI](http://before.com.br)

Cargo: Front-end Enginner

**10/2018** - **02/2019** - [Quality Sistemas](http://qualitysistemas.com.br)

Cargo: Desenvolvedor Web

**2012** - **09/2018** - [Cartório Ayache](http://cartorioayache.com.br)

Cargo: Escrevente | Desenvolvedor Web

**2006** - **02/2012** - Cartório Pedra

Cargo: Escrevente


### Idioma
Português - Fluente

Inglês - Técnico

### Informações Adicionais
 - Curso Bootcamp Rocketseat - [Cursando]


 - Curso de Typescript - [Certificado](https://gitlab.com/paulopanissa/curriculum/blob/3a028e266dc3825cc73ae27ded9b2e18569b38c9/certificados/certificate-typescript.jpg)
 - Curso de Gulp - [Certificado](https://gitlab.com/paulopanissa/curriculum/blob/3a028e266dc3825cc73ae27ded9b2e18569b38c9/certificados/certificate-gulp.jpg)
 - Curso de Iniciando com Node.JS - [Certificado](https://gitlab.com/paulopanissa/curriculum/blob/3a028e266dc3825cc73ae27ded9b2e18569b38c9/certificados/certificate-iniciando-com-nodejs.jpg)
 - Curso de Node com ExpressJS - [Certificado](https://gitlab.com/paulopanissa/curriculum/blob/3a028e266dc3825cc73ae27ded9b2e18569b38c9/certificados/certificate-node-com-expressjs.jpg)
 - Curso de Socket.io - Apps Realtime - [Certificado](https://gitlab.com/paulopanissa/curriculum/blob/3a028e266dc3825cc73ae27ded9b2e18569b38c9/certificados/certificate-socketio-apps-realtime.jpg)
 - Curso de Plataforma de Chat com Node.js e Socket.io - [Certificado](https://gitlab.com/paulopanissa/curriculum/blob/3a028e266dc3825cc73ae27ded9b2e18569b38c9/certificados/certificate-plataforma-de-chat-com-nodejs-e-socketio.jpg)
 - Curso de VueJS 1 - [Certificado](https://gitlab.com/paulopanissa/curriculum/blob/3a028e266dc3825cc73ae27ded9b2e18569b38c9/certificados/certificate-vuejs.jpg)
 - Curso de VueJS 2 - [Certificado](https://gitlab.com/paulopanissa/curriculum/blob/3a028e266dc3825cc73ae27ded9b2e18569b38c9/certificados/certificate-vuejs-20.jpg)
 - Curso de Vue 2.0 com Vuex - [Certificado](https://gitlab.com/paulopanissa/curriculum/blob/3a028e266dc3825cc73ae27ded9b2e18569b38c9/certificados/certificate-vue-20-com-vuex.jpg)
 - Curso de API com NodeJS, Restify, Mongoose, MongoDB e use o Gitlab para Automatizar o Deploy na Digital Ocean: [Certificado](https://www.udemy.com/certificate/UC-W9H90SL9/)
 - Curso de Angular 2 - Primeiros Passos - [Certificado](https://gitlab.com/paulopanissa/curriculum/blob/3a028e266dc3825cc73ae27ded9b2e18569b38c9/certificados/certificate-angular-2-0-primeiros-passos.jpg)
 - Curso de Web Crawlers com NodeJS - [Certificado](https://gitlab.com/paulopanissa/curriculum/blob/3a028e266dc3825cc73ae27ded9b2e18569b38c9/certificados/certificate-web-crawler-com-node.jpg)
- Iniciando com Angular 7 - [Certificado](https://www.schoolofnet.com/validar-certificado/?certificate=1e959c2a-5db0-4ec7-abae-39d5df59e522)
- Angular 7 Componentes - [Certificado](https://www.schoolofnet.com/validar-certificado/?certificate=9ed1f535-f3ac-4aab-a602-adf66a6d7ee7)
- Iniciando com React.JS [Certificado](https://www.schoolofnet.com/validar-certificado/?certificate=31a56359-9bc2-4c6b-ab6d-11e8137c20e8)
- React com Redux [Certificado](https://www.schoolofnet.com/validar-certificado/?certificate=21d93b02-45ce-46e5-806e-7510d245740c)
- Avançando com React - Redux Forms [Certificado](https://www.schoolofnet.com/validar-certificado/?certificate=a82931b1-28b0-4c86-b9e6-de4d19d361b8)
 
### Github
https://github.com/paulopanissa


 